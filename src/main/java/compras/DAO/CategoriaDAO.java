package compras.DAO;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import compras.modelo.Categoria;

public class CategoriaDAO implements BasicDAO<Categoria> {
	
	private EntityManager em = getInstance();
	private EntityTransaction tx;
	
	public EntityManager getInstance() {
		if (em == null) {
			return EMFactory.factory.createEntityManager();
		}
		return em;
	}

	public Categoria get(int id) {
		Categoria categoria = em.find(Categoria.class, id);
		if (categoria != null) {
			return categoria;
		}
		return null;
	}

	public List<Categoria> getAll() {
		Query query = em.createQuery("SELECT c FROM Categoria c");
		return query.getResultList();
	}

	public boolean save(Categoria categoria) {
		return ejecutarEnTransaccion(em -> em.persist(categoria));
	}

	public boolean update(Categoria categoria) {
		return ejecutarEnTransaccion(em -> em.merge(categoria));
	}

	public boolean delete(Categoria categoria) {
		Categoria categoriaMergeada = em.merge(categoria);
		return ejecutarEnTransaccion(em -> em.remove(categoriaMergeada));
	}
	
	private boolean ejecutarEnTransaccion(Consumer<EntityManager> accion) {
        tx = em.getTransaction();
        try {
            tx.begin();
            accion.accept(em);
            tx.commit();
            return true;
        }
        catch (RuntimeException e) {
            tx.rollback();
            throw e;
        }
    }
}
