package compras.DAO;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EMFactory {
	static final String PERSISTENCE_UNIT = "Compras-PU";
	public static EntityManagerFactory factory = 
			Persistence.createEntityManagerFactory(PERSISTENCE_UNIT);
}
