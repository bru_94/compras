package compras.interfaces;

import java.util.List;

import compras.modelo.Categoria;

public interface ICategoria {
	Categoria get(int id);
	
	List<Categoria> getAll();
	
	boolean save(Categoria categoria);
	
	boolean update(Categoria categoria);
	
	boolean delete(Categoria categoria);
}