package compras.controladores;

import java.util.List;

import compras.DAO.CategoriaDAO;
import compras.interfaces.ICategoria;
import compras.modelo.Categoria;

public class ControladorCategoria implements ICategoria {
	
	private static CategoriaDAO categoriaDAO = new CategoriaDAO();
	
	public Categoria get(int id) {
		return categoriaDAO.get(id);
	}
	
	@Override
	public List<Categoria> getAll() {
		return categoriaDAO.getAll();
	}

	@Override
	public boolean save(Categoria categoria) {
		return categoriaDAO.save(categoria);
	}

	@Override
	public boolean update(Categoria categoria) {
		if (get(categoria.getId()) != null) {
			return categoriaDAO.update(categoria);
		}
		return false;
	}

	@Override
	public boolean delete(Categoria categoria) {
		return categoriaDAO.delete(categoria);
	}
}
