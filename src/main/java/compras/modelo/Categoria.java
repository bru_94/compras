package compras.modelo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "categoria")
public class Categoria {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int categoriaId;
	private String nombre;
	
	public Categoria() {
		
	}
	
	public Categoria(String nombre) {
		this.nombre = nombre;
	}

	public int getId() {
		return categoriaId;
	}

	public String getNombre() {
		return nombre;
	}

	public void setId(int id) {
		this.categoriaId = id;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}	
}
