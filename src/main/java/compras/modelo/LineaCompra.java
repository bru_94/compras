package compras.modelo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "lineacompra")
public class LineaCompra {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int lineaCompraId;
	@ManyToOne
	@JoinColumn(name = "compraId")
	private Compra compra;
	@ManyToOne
	@JoinColumn(name = "productoId")
	private Producto producto;
	private float precio;
	private int cantidad;
	private float subtotal;
	
	public LineaCompra() {
		
	}
	
	public LineaCompra(Compra compra, Producto producto, float precio, int cantidad) {
		this.compra = compra;
		this.producto = producto;
		this.precio = precio;
		this.cantidad = cantidad;
		this.subtotal = precio*cantidad;
	}
	
	public Compra getCompra() {
		return compra;
	}
	
	public int getLineaCompraId() {
		return lineaCompraId;
	}

	public Producto getProducto() {
		return producto;
	}

	public float getPrecio() {
		return precio;
	}

	public int getCantidad() {
		return cantidad;
	}
	
	public float getSubTotal() {
		return subtotal;
	}
	
	public void setCompra(Compra compra) {
		this.compra = compra;
	}
	
	public void setlineaCompraId(int id) {
		this.lineaCompraId = id;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public void setPrecio(float precio) {
		this.precio = precio;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	
	public void setSubTotal(int cantidad, float precio) {
		this.subtotal = precio*cantidad;
	}
}
