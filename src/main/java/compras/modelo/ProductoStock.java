package compras.modelo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "productostock")
public class ProductoStock {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int productoStockId;
	@ManyToOne
	@JoinColumn(name = "productoId")
	private Producto producto;
	private float ultimoPrecio;
	private int cantidadRestante;
	
	public ProductoStock() {
		
	}
	
	public ProductoStock(Producto producto, float ultimoPrecio, int cantidadRestante) {
		this.producto = producto;
		this.ultimoPrecio = ultimoPrecio;
		this.cantidadRestante = cantidadRestante;
	}
	
	public int getproductoStockId() {
		return productoStockId;
	}

	public Producto getProducto() {
		return producto;
	}

	public float getUltimoPrecio() {
		return ultimoPrecio;
	}

	public int getCantidadRestante() {
		return cantidadRestante;
	}

	public void setProductoStockId(int id) {
		this.productoStockId = id;
	}
	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public void setUltimoPrecio(float ultimoPrecio) {
		this.ultimoPrecio = ultimoPrecio;
	}

	public void setCantidadRestante(int cantidadRestante) {
		this.cantidadRestante = cantidadRestante;
	}
}
