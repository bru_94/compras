package compras.modelo;

import javax.persistence.Embeddable;

@Embeddable
public class Ubicacion {
	private long latitud;
	private long longitud;
	
	public Ubicacion() {
		
	}
	
	public Ubicacion(long latitud, long longitud) {
		this.latitud = latitud;
		this.longitud = longitud;
	}
	
	public long getLatitud() {
		return latitud;
	}
	public long getLongitud() {
		return longitud;
	}
	public void setLatitud(long latitud) {
		this.latitud = latitud;
	}
	public void setLongitud(long longitud) {
		this.longitud = longitud;
	}
}
