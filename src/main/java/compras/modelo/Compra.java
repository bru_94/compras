package compras.modelo;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "compra")
public class Compra {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int compraId;
	@ManyToOne
	@JoinColumn(name = "localId")
	private Local local;
	@OneToMany(mappedBy = "producto")
	private List<LineaCompra> productos;
	private float total;
	
	public Compra() {
		
	}
	
	public Compra(Local local) {
		this.local = local;
		this.productos = new ArrayList<LineaCompra>();
		this.total = 0;
	}
	
	public int compraId() {
		return compraId;
	}

	public Local getLocal() {
		return local;
	}

	public List<LineaCompra> getProductos() {
		return productos;
	}

	public float getTotal() {
		return total;
	}
	
	public void compraId(int id) {
		this.compraId = id;
	}

	public void setLocal(Local local) {
		this.local = local;
	}

	public void setProductos(List<LineaCompra> productos) {
		this.productos = productos;
	}

	public void setTotal(float total) {
		this.total = total;
	}
}
