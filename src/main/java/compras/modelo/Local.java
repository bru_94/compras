package compras.modelo;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "local")
public class Local {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int localId;
	private String nombre;
	
	@Embedded
	private Ubicacion ubicacion;
	
	public Local() {
		
	}

	public Local(String nombre, Ubicacion ubicacion) {
		this.nombre = nombre;
		this.ubicacion = ubicacion;
	}

	public int getLocalId() {
		return localId;
	}

	public String getNombre() {
		return nombre;
	}

	public Ubicacion getUbicacion() {
		return ubicacion;
	}

	public void setlocalId(int id) {
		this.localId = id;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setUbicacion(Ubicacion ubicacion) {
		this.ubicacion = ubicacion;
	}
}
