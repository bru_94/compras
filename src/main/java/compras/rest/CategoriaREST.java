package compras.rest;


import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import compras.controladores.ControladorCategoria;
import compras.interfaces.ICategoria;
import compras.modelo.Categoria;

@Path("/categorias")
public class CategoriaREST {
	private ICategoria ICategoria = new ControladorCategoria();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public Response getCategoriaById(@PathParam("id") int id) {
		Categoria categoria = ICategoria.get(id);
		if (categoria != null) {
			return Response
					.status(Status.OK)
					.entity(categoria)
					.build();
		}
		return Response
				.status(Status.NOT_FOUND.getStatusCode(), "No existe categoria con ese id")
				.build();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCategorias() {
		return Response.ok().entity(ICategoria.getAll()).build();
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response saveCategoria(Categoria categoria) {
		if (ICategoria.save(categoria)) {
			return Response.status(Status.CREATED).build();
		}
		return Response.status(Status.INTERNAL_SERVER_ERROR).build();
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public Response updateCategoria(Categoria categoria, @PathParam("id") int id) {
		categoria.setId(id);
		if (ICategoria.update(categoria)) {
			return Response.status(Status.OK).build();
		}
		return Response.status(Status.INTERNAL_SERVER_ERROR).build();
	}
	
	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public Response deleteCategoria(Categoria categoria, @PathParam("id") int id) {
		categoria.setId(id);
		if (ICategoria.delete(categoria)) {
			return Response.status(Status.OK).build();
		}
		
		return Response.status(Status.INTERNAL_SERVER_ERROR).build();
	}
}
